class Mahasiswa extends Orang {
    private String universitas;

    public Mahasiswa(String nama, int umur, String universitas) {
        super(nama, umur);
        this.universitas = universitas;
    }

    public void infoUniversitas() {
        System.out.println("Saya kuliah di " + universitas);
    }
}