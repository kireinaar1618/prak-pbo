class Orang {
    private String nama;
    private int umur;

    public Orang(String nama, int umur) {
        this.nama = nama;
        this.umur = umur;
    }

    public void sapaan() {
        System.out.println("Halo, nama saya " + nama);
    }

    public void infoUmur() {
        System.out.println("Saya berusia " + umur + " tahun");
    }
}
