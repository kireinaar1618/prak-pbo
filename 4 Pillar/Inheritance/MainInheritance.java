public class MainInheritance {
    public static void main(String[] args) {
        Orang orang1 = new Orang("Andi", 25);
        orang1.sapaan();
        orang1.infoUmur();

        Mahasiswa mahasiswa1 = new Mahasiswa("Budi", 20, "Universitas Gadjah Mada");
        mahasiswa1.sapaan();
        mahasiswa1.infoUmur();
        mahasiswa1.infoUniversitas();
    }
}