abstract class Hewan {

    public abstract void suara();
 
    public void tidur() {
       System.out.println("Suara tidur = Zzz...");
    }
 }
 
 class Sapi extends Hewan {
    public void suara() {
       System.out.println("Suara sapi = Mooo!");
    }
 }
 
 class Bebek extends Hewan {

    public void suara() {
       System.out.println("Suara bebek = Wek wek!");
    }
 }
 
 public class MainAbstrak {
    public static void main(String[] args) {
       Hewan hewan1 = new Sapi();
       Hewan hewan2 = new Bebek();
 
       hewan1.suara(); // Output: "Mooo!"
       hewan2.suara(); // Output: "Wek wek!"
 
       hewan1.tidur(); // Output: "Zzz..."
       hewan2.tidur(); // Output: "Zzz..."
    }
 }
 