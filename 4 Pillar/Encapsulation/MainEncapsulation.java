public class MainEncapsulation {
    public static void main(String[] args) {
        Mahasiswa mhs1 = new Mahasiswa("Budi", 20, "Teknik Informatika");
        mhs1.printDetails();

        mhs1.setUmur(21);
        mhs1.printDetails();
    }
}