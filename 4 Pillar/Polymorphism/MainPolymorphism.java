public class MainPolymorphism  {
    public static void main(String[] args) {
        Kendaraan kendaraan1 = new Kendaraan();
        Kendaraan kendaraan2 = new Mobil();
        Kendaraan kendaraan3 = new Motor();

        kendaraan1.info();
        kendaraan2.info();
        kendaraan3.info();
    }
}